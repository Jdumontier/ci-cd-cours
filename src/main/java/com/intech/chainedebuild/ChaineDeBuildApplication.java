package com.intech.chainedebuild;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChaineDeBuildApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChaineDeBuildApplication.class, args);
	}

}
